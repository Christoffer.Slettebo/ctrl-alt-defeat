# Rapport – innlevering 3
Team: CRTL+ALT+DEFEAT

# Medlemer:
* Teamlead: Christoffer A. Slettebø
* Programmerer/Nestleder: Olav Høysæther Opheim
* Programmerer/Kundekontakt: Marius Soma
* Programmerer/Testansvarlig: Henrik Johansen

# Hvordan fungerer rollene?
Olav har som nestleder også vært sekretær, dette fungerer bra siden det blir lik struktur på alle referat
Teamlead rollen har ikke betydd så mye siden vi har gjort valg i lag og forsåvidt sam kjørt
Lite kunder så ikke mye action for kundekontakt
Vi har alle funnet feil og testkjørt, så rollene betyr ikke så mye for oss
Kommunikasjonen i gruppen er god, lett å komme fram til tidspunkt alle kan møte


# MVP (Ferdig):
* Vise et spillebrett
* Vise spiller på spillebrett
* Flytte spiller ved å bruke tastene
* Spiller interagerer med terreng
* Vise fiender/monstre; de skal interagere med terreng og spiller
* Spiller kan dø (ved kontakt med fiender, eller ved å falle utfor skjermen)
* Mål for spillbrett, å komme i mål uten å dø
* Start-skjerm ved oppstart / game over

# Early access-version (Dette er de vi har lagt til):
* Power up (Marius kan drikke pepsi og vokse seg stor)
* To fiender (en edderkopp og et pepsi turtle som kan dø og drepe marius)
* Musikk
* Et nytt map
* oppdatert pixel art
* Restruktur
* Tester
* Main menu og Help menu

# Hva kan gjøres bedre?
* Vi må få til å lage mer avanserte tester
* Rydde og strukturere filene

# Bugs som vi har funnet:
* Kan "sveve" ved å spamme hoppe knapp når man treffer en blokk i lufta
* Kan "sveve" ved å spamme venstre eller høgre knapp intil en vegg/blokk
* Hit box rar på enemies, spesielt når man er stor
* Faller man fra en stor høyde ned på enemies kan også skade selv om man treffer hodet. 
* Mens man går fra liten marius til stor marius ser det ut som man kan dobbel jumpe mid animation
* Spillet krasjer når en tar en coin, en dreper en spider, tar en pepsi og så før pepsien er i bakken tar en coin. 
  Får da Java Runtime Enviroment feil. (Muligens CoinAnimation feil)

# Til neste innlevering skal vi legge til (final product):
* Legge til mulighet for å velge mellom maps
* Lage minst 1 nye maps (slik at vi har 3 totalt)
* En ny fiende
* Legge til en ny power up (ikkje komt frem til hva enda)
* Legge til sound når man dør, drikker pepsi etc.
* Få til minst 75% coverage
* Lage jar fil
* Sørge for at spillet fungerer på alle platformer det skal fungere på


# Commit
* Vi har alle commitet en del
* Vi har jobbet litt i brancher, men og pushet en del til main da vi har jobbet mer sammen for å løse problemer
* Noen er litt mer glad i flere små commits, mens noen i få store


# Rapporter
* uke11.md
* uke12.md
* uke13.md
* uke14.md
* uke15.md

# Her er 2 forbedringspunkt
* Testing - Lage mer avanserte tester
* Javadocs - Skrive mer javadocs underveis

# Prosess for teamet videre:
Vi jobber bra som team og syntes at digitale møter fungerer fint, og går på fysisk gruppe time hvis vi har spørsmål eller ting vi trenger hjelp med. Vi må få lagd litt mer javadocs og fortsette med tester, og få testet enda mer slik at vi får mist 75% test coverage.

# Oppsumering
Vi har komt et stykke videre fra MVP versionen og er fornøyed med fremgangen vi har hatt. Vi slet litt med å komme i gang med tester, men når vi fikk lagd til mocito som dependencies og lest oss litt opp så fikk vi komt i gang med litt tester. Vi jobber bra som et team der vi har hyppige møter, og har en god plan for hvordan vi skal komme i mål med final product versionen av spillet.
