Møtested: Vilvite (gruppetime)
Dato: 13. Februar
Oppmøte: Alle i gruppen!

Dette gjorde vi på gruppetime:
- Fullførte oblig 1, del A1 - A5
- Diskuterte og kom frem til videre konsept for spillet vi skal utvikle
- Kom frem til at vi skal møtes en gang i uken på gruppe time (+ over discord ved behov)
- Lagd en plan for hvordan vi skal jobbe fremmover og metodikk; Bruk av Trello, parprogrammering og testing
