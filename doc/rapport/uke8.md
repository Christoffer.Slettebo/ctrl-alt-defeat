Møtested: Vilvite (gruppetime)
Dato: 20. Februar
Oppmøte: Alle i gruppen!

Dette gjorde vi på gruppetime:

- Bestemt oss for å bruke branches i git til programmering, og merge branchesene når vi møtes på gruppetimene hver uke
- Planlagt å få til å bevege en karakter i spillet iløpet av denne eller neste uke
- Marius har begynt å lage spill brettet og se på hvordan vi kan implemitere dette inn i spillet
- Vi ander har sett på skeleton koden, og også begynnt å legge en plan for hvordan vi skal gå frem å utivkle spillet videre med mapper, klasser osv.

Dette skal vi gjøre neste uke (uke 9):

- Digitalt møte istede for fysisk
- Gå gjennom branches og merge de til main
- Jobbe videre med å bevege en karakter i spillet (hvis ferdig prøve å få tegnet mappet i spillet, og få det til å interagere med spillere).
