Møtested: Digitalt (Discord)
Dato: 19. April
Oppmøte: Alle i gruppen!

Dette gjorde vi på gruppetime:
- Vi gjekk på gruppe time for å få litt hjelp med testing (men hadde og et digitalt møte på discord)
- Vi fikk till å vise coverage og fikset probleme der buildet alltid feilet (vi hadde ingen aktive runners)
- Lagde et par tester og har lest oss mer opp på mockito, har kunn 1% coverage så vi har en del jobb å gjøre der

Dette skal vi forsette å gjøre denne uken:
- Vi har en del innleveringer denne uken så vi tar helg for denne gang og tenker å jobbe mer neste uke


Dette gjør vi neste uke (16):
- Skrive masse tester for å komme nærmere 75% coverage
- Implementere de siste tingene som trengs å implementeres
- Rydde opp i filer og skrive javadoc
- Oppdatere README filen og andre tekst filer som trengs
