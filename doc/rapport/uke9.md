Møtested: Digitalt (Discord)
Dato: 29. Februar
Oppmøte: Alle i gruppen!

Dette gjorde vi på gruppetime:
- Sett på hvordan vi skal få til event handlers i spillet, og begynt å implemitere det
- Nesten ferdig med føreste kartet, og begynt å sett litt på hvordan vi skal få implementert dette inn i spillet
- Diskutert litt og sett på hva folk har fått gjort siden forgje gang vi møttes
- Oppdatert og lagt inn nye ting i trello

Dette skal vi gjøre neste uke (uke 10):
- Møte Mandag, Tirsdag og en dag til for å komme i mål med innlevering (prototype-version)
- Parprogrammering når vi jobber