# Rapport – innlevering 2
**Team:** CRTL+ALT+DEFEAT

# Medlemer:
* Teamlead: Christoffer A. Slettebø
* Programmerer/Nestleder: Olav Høysæther Opheim
* Programmerer/Kundekontakt: Marius Soma
* Programmerer/Testansvarlig: Henrik Johansen

# Hvordan fungerer rollene?
* Olav har som nestleder også vært sekretær, dette fungerer bra siden det blir lik struktur på alle referat
* Teamlead rollen har ikke betydd så mye siden vi har gjort valg i lag og forsåvidt sam kjørt
* Lite kunder så ikke mye action for kundekontakt
* Vi har alle funnet feil og testkjørt, så rollene betyr ikke så mye for oss
* Kommunikasjonen i gruppen er god, lett å komme fram til tidspunkt alle kan møte

# MVP:
* Vise et spillebrett
* Vise spiller på spillebrett
* Flytte spiller ved å bruke tastene
* Spiller interagerer med terreng
* Vise fiender/monstre; de skal interagere med terreng og spiller
* Spiller kan dø (ved kontakt med fiender, eller ved å falle utfor skjermen)
* Mål for spillbrett, å komme i mål uten å dø
* Start-skjerm ved oppstart / game over
* Brukerhistorier:
* Her er eksempel på to bruker historier som passer for spiller vi skal utvikle,

# Hva er klart til nå?
* Spillet skal være forsvådit veldig spillbart
* Vi har lagt til en bane
* Marius/mario kan dø
* Marius/mario kan hoppe og ødellege blokker
* MArius/mario skal kunne vinne, men er ikke helt ferdig enda
* Jobbes med å få inn fiender
* Jobbes med defeat og victory screen

# Kan gjøres bedre?
* Kan lages flere tester
* Vi må nok rydde litt i filene og strukturen
* Vi har litt ekstra med som vi prøvde på men som nok må vekk
* Bugs som vi har funnet:
* * kan hoppe for alltid etter hodekollisjon.
* * Til neste innlevering skal vi lage eget map og endre mer på Marius slik den ikkje ser ut som mario.
* * Legge til flere enemies.


# Commit
* Vi har alle commitet en del
* Kan være litt variasjon siden vi har jobbd i brancher
* Noen er litt mer glad i flere små commits, mens noen i få store

# Rapporter
* uke7.md
* uke8.md
* uke9.md
* uke10.md

# våre 3 forbedringspunkter
* Struktur
* Javadocs
* Testing

# Prosess for teamet videre:
* Vi skal jobbe med å få bedre struktur. Vi mangler mye javadocs og tester. Vi skal jobbe videre med parprogrammering,
* Vi har også opplevd god suksess med digitale møter. 
* det siden vi finner fort små feil til de andre. Vi kommer til å fortsette med å få i gruppetimene, det siden alle kan da. 

# Oppsumering
* Vi har kommet langt, vi har fått til ca alle målene våre for MVP, kanskje utenom å kunne gjøre noe med fiendene. 
* Vi opplevde at først vi mangler noen ekstra bibliotek til gdx som box2d, men når vi fikk det på plass gikk det kjappt mye bedre.
* Vi hadde kanskje tenkt å ha fungerende fiender, men vi kom ikke så langt denne gangen
* Vi at flere små møter digitalt som hjelpsomt 

